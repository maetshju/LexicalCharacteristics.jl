using Documenter, LexicalCharacteristics

makedocs(sitename = "LexicalCharacteristics.jl",
         pages = ["index.md", "pnd.md", "upt.md", "phnprb.md", "textvptree.md"],
         repo = "https://gitlab.com/maetshju/LexicalCharacteristics.jl")
