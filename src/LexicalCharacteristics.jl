module LexicalCharacteristics

export
    pnd,
    lev,
    upt,
    phnprb,
    radiusSearch,
    nneighbors,
    TextVPTree

include("upt.jl")
include("pnd.jl") #includes textvptree.jl
include("phnprb.jl")
end
