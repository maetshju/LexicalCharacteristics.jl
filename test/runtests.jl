using Test
using LexicalCharacteristics
using DataFrames

sample_corpus = [
["K", "AE1", "T"], # cat
["K", "AA1", "B"], # cob
["B", "AE1", "T"], # bat
["T", "AE1", "T", "S"], # tats
["M", "AA1", "R", "K"], # mark
["K", "AE1", "B"], # cab
]

# Test upt

## Unique at second character
desired = DataFrame(Query=[["K", "AA1", "B"]], UPT=[2])
@test upt(sample_corpus, [["K", "AA1", "B"]]; inCorpus=true) == desired

## Unique at first character
desired = DataFrame(Query=[["HH", "AE1", "T"]], UPT=[1])
@test upt(sample_corpus, [["HH", "AE1", "T"]]; inCorpus=false) == desired

## Unique at the last character
desired = DataFrame(Query=[["K", "AE1", "D"]], UPT=[3])
@test upt(sample_corpus, [["K", "AE1", "D"]]; inCorpus=false) == desired

## Not unique
desired = DataFrame(Query=[["T", "AE1", "T"]], UPT=[4])
@test upt(sample_corpus, [["T", "AE1", "T"]]; inCorpus=false) == desired

# Test pnd

## Showing progress

### Neighbors with bat and cab
desired = DataFrame(Query=[["K", "AE1", "T"]], PND=[2])
@test pnd(sample_corpus, [["K", "AE1", "T"]]; progress=true) == desired

### Neighbors with cat and bat
desired = DataFrame(Query=[["HH", "AE1", "T"]], PND=[2])
@test pnd(sample_corpus, [["HH", "AE1", "T"]]; progress=true) == desired

### No neighbors
desired = DataFrame(Query=[["L", "IH1", "V"]], PND=[0])
@test pnd(sample_corpus, [["L", "IH1", "V"]]; progress=true) == desired

## Not showing progress

### Neighbors with bat and cab
desired = DataFrame(Query=[["K", "AE1", "T"]], PND=[2])
@test pnd(sample_corpus, [["K", "AE1", "T"]]; progress=false) == desired

### Neighbors with cat and bat
desired = DataFrame(Query=[["HH", "AE1", "T"]], PND=[2])
@test pnd(sample_corpus, [["HH", "AE1", "T"]]; progress=false) == desired

### No neighbors
desired = DataFrame(Query=[["L", "IH1", "V"]], PND=[0])
@test pnd(sample_corpus, [["L", "IH1", "V"]]; progress=false) == desired

# Test phnprb

#=
**Co-occurrence**

20 total monophones
K:      4
AE1:    4
T:      4
S:      1
M:      1
AA1:    2
B:      3
R:      1

["K", "AE1", "T"], get counts
[4, 4, 4], divide by total monophones
[0.2, 0.2, 0.2], take product
0.2^3 = 0.008
=#
freq = [1,1,1,1,1,1]
p = prod([4,4,4] / 20)
desired = DataFrame(Query=[["K", "AE1", "T"]], Probability=[p])
prb = phnprb(sample_corpus, freq, [["K", "AE1", "T"]])
@test prb == desired

#=

**Co-occurrence diphones**

26 total diphones
. K:    3
K  AE1: 2
AE1 T:  3
T .:    2
K AA1:  1
AA1 B:  1
B .:    2
. B:    1
B AE1:  1
. T:    1
T AE1:  1
T S:    1
S .:    1
. M:    1
M AA1:  1
AA1 R:  1
R K:    1
K .:    1
AE1 B:  1

[(., K), (K, AE1), (AE1, T), (T .)]
[3, 2, 3, 2]
[3/26, 2/26, 3/26, 2/26]
36/(26^4) = 7.9e-5
=#
p = prod([3,2,3,2]/26)
desired = DataFrame(Query=[["K", "AE1", "T"]], Probability=[p])
prb = phnprb(sample_corpus, freq, [["K", "AE1", "T"]]; nchar=2)
@test prb == desired

#=
**Co-occurrence diphones, no padding**

14 total diphones
K  AE1: 2
AE1 T:  3
K AA1:  1
AA1 B:  1
B AE1:  1
T AE1:  1
T S:    1
M AA1:  1
AA1 R:  1
R K:    1
AE1 B:  1

[(K, AE1), (AE1, T)]
[2, 3]
[3/14, 2/14] # take product
6/(14^2) = 7.9e-5
=#
p = prod([3,2]/14)
desired = DataFrame(Query=[["K", "AE1", "T"]], Probability=[p])
prb = phnprb(sample_corpus, freq, [["K", "AE1", "T"]]; nchar=2, pad=false)
@test prb == desired

#=
**Positional*

20 total monophones
K 1:    3
AE1 2:  4
AA1 2:  2
B 3:    2
B 1:    1
T 3:    3
T 1:    1
S 4:    1
M 1:    1
R 3:    1
K 4:    1

["K", "AE1", "T"], get counts
[3, 4, 3], divide by total monophones
[3/20, 4/20, 3/20], take product
36/8000 = 0.0045
=#
p = prod([3,4,3]/20)
desired = DataFrame(Query=[["K", "AE1", "T"]], Probability=[p])
prb = phnprb(sample_corpus, freq, [["K", "AE1", "T"]]; positional=true)
@test prb == desired

## Phone not in corpus
p = 0
desired = DataFrame(Query=[["K", "AE1", "F"]], Probability=[p])
@test phnprb(sample_corpus, freq, [["K", "AE1", "F"]]) == desired

## With non-1 frequencies; doubling each frequency should produce same output
freq = [2,2,2,2,2,2]
p = prod([8,8,8] / 40)
desired = DataFrame(Query=[["K", "AE1", "T"]], Probability=[p])
prb = phnprb(sample_corpus, freq, [["K", "AE1", "T"]])
@test prb == desired

# Test TextVPTree
tree = TextVPTree(sample_corpus, lev)

## Test nneighbors
res = collect(keys(nneighbors(tree, ["K", "AE1", "T"], 2)))
res = sort(join.(res, " "))
@test res == ["B AE1 T", "K AE1 B"]

### Test on point not inside of the tree
res = collect(keys(nneighbors(tree, ["HH", "AE1", "T"], 2)))
res = sort(join.(res, " "))
@test res == ["B AE1 T", "K AE1 T"]

## Test radius search
res = radiusSearch(tree, ["K", "AE1", "T"], 1)
res = sort(join.(res, " "))
@test res == ["B AE1 T", "K AE1 B"]

### Test on point not inside of the tree
res = radiusSearch(tree, ["HH", "AE1", "T"], 1)
res = sort(join.(res, " "))
@test res == ["B AE1 T", "K AE1 T"]
