# Version 0.2.1

* Fixed bug in pnd calculation that identified items with Levenshtein distance of 1 from the query as neighbors

# Version 0.2.0

* Updated CI to work with Julia 1.x
* Added optional progress meter for pnd

# Versions before 0.2.0

Changes not documented in changelog. Initial functionality for phonological neighborhood density, phonotactic probability, uniqueness point, and TextVPTree provided. Worked on Julia 0.6, then updated to 0.7/1.0 syntax/function names.
