# LexicalCharacteristics

A Julia package that calculates various lexical characteristics for experiments in phonetics and psycholinguistics. Currently, the package will calculate phonological neighborhood density, uniqueness point, and phonotactic probability. It also provides access to the vantage point tree used in the calculation of the phonological neighborhood density, for use in other tasks that require a vantage point tree. This vantage point tree implementation has only been tested to work for text data, and specifically for calculating the phonological neighborhood density, however.

[Check out the documentation](https://maetshju.gitlab.io/LexicalCharacteristics.jl/) for examples of usage.

## Installation

This package is currently not registered, so it should be installed from this Git repository.

```julia
] add "https://gitlab.com/maetshju/LexicalCharacteristics.jl.git"
```
